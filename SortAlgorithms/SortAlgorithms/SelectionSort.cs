﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortAlgorithms
{
    public class SelectionSort
    {
        private int[] sortArray;

        public SelectionSort() { }

        public int[] Sort(int[] array)
        {
            sortArray = array;

            for (int i = 0; i < array.Length; i++)
            {
                for (int j = i; j < array.Length; j++)
                {
                    if (array[j] < array[i])
                    {
                        Swap(i, j);
                    }
                }
            }
            return array;
        }

        private void Swap(int i, int j)
        {
            int temp = sortArray[i];
            sortArray[i] = sortArray[j];
            sortArray[j] = temp;
        }
    }
}
