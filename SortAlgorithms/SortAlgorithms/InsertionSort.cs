﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortAlgorithms
{
    public class InsertionSort
    {
        public InsertionSort() { }

        public int[] GetSorted(int[] array)
        {
            int n = array.Length;
            array = Sort(array, n-1);
            return array;
        }

        private int[] Sort(int[] array, int n)
        {
            if (n > 0)
            {
                array = Sort(array, n-1);
                int x = array[n];
                int j = n - 1;
                while (j >= 0 && array[j] > x)
                {
                    array[j + 1] = array[j];
                    j = j - 1;
                }
                array[j + 1] = x;
            }
            return array;
        }
    }
}
