﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortAlgorithms
{
    public class BubbleSort
    {
        public BubbleSort() { }

        public int[] Sort(int[] array) {

            int length = array.Length;
            int swapped = 0;
            int i = 0;


            while (true)
            {
                i = i + 1;
                if (array[i-1] > array[i])
                {
                    array = Swap(array, i-1, i);
                    swapped = 1;
                }
                if (i == length - 1 && swapped == 0)
                {
                    return array;
                }
                if (i == length - 1)
                {
                    i = 0;
                    swapped = 0;
                }
                
            }
        }

        public int[] Swap(int[] array, int i, int j)
        {
            int temp = array[i];
            array[i] = array[j];
            array[j] = temp;
            return array;
        }
    }
}
