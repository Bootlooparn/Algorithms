﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortAlgorithms
{
    public class QuickSort
    {
        private int[] sortArray;

        public QuickSort() {}
        /* Using Lomuto partition scheme */

        public int[] Sort(int[] array) {
            int high = array.Length - 1;
            int low = 0;
            sortArray = array;

            if (low < high)
            {
                int p = Partition(low, high);
                Sort(low, p);
                Sort(p + 1, high);
            }

            return array;
        }

        private void Sort(int low, int high) {
            if (low < high)
            {
                int p = Partition(low, high);
                Sort(low, p);
                Sort(p + 1, high);
            }
        }

        private int Partition(int low, int high) {
            int pivot = sortArray[low];
            int i = low;
            int j = high;

            while (true)
            {
                while (sortArray[i] < pivot)
                {
                    i = i + 1;
                }
                while (sortArray[j] > pivot)
                {
                    j = j - 1;
                }
                if (i >= j)
                {
                    return j;
                }
                Swap(i, j);
            }
        }

        private void Swap(int i, int j) {
            int temp = sortArray[i];
            sortArray[i] = sortArray[j];
            sortArray[j] = temp;
        }
    }
}
