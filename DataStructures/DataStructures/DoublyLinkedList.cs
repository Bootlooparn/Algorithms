﻿using Utilities;
using System;

namespace DataStructures
{
    public class DoublyLinkedList
    {
        private Node start = null;

        public DoublyLinkedList() { }

        public void Add(object info)
        {
            Node n = new Node(info);

            if (start != null)
            {
                start.SetPrev(n);
                n.SetNext(start);
                start = n;
            }
            else
            {
                start = n;
            }
        }
        public void AddBefore(object info, object objectInfo)
        {
            if (start.GetInfo() == objectInfo)
            {
                Node newNode = new Node(info);
                newNode.SetNext(start);

                start = newNode;
            }
            else
            {
                Node n = start;

                while (n != null)
                {
                    if (n.GetInfo() == objectInfo)
                    {
                        Node newNode = new Node(info);

                        newNode.SetPrev(n.GetPrev());
                        n.GetPrev().SetNext(newNode);
                        n.SetPrev(newNode);
                        newNode.SetNext(n);
                        break;
                    }
                    n = n.GetNext();
                }
            }
        }
        public void AddAfter(object info, object objectInfo)
        {

        }
        public void AddIndex(object info, int index) { }
        public void Remove(object info) { }
        public void Reverse() { }
        public void ShowList()
        {
            Node n = start;

            while (n != null)
            {
                Console.WriteLine(n.GetInfo());
                n = n.GetNext();
            }
        }
    }
}
