﻿using System;
using Utilities;

namespace DataStructures
{
    public class SingleLinkedList
    {
        private Node start = null;

        public SingleLinkedList() {}

        public void Add(object info)
        {
            Node n = new Node(info);

            n.SetNext(start);
            start = n;
        }

        public void Add(object info, object nodeInfo)
        {

            Node n = start;

            while (n.GetInfo() != null)
            {
                if (n.GetInfo() == nodeInfo)
                {
                    Node newNode = new Node(info);
                    newNode.SetNext(n.GetNext());
                    n.SetNext(newNode);
                    break;
                }
                else
                {
                    n = n.GetNext();
                }
            }
        }

        public void Remove()
        {

            start = start.GetNext();
        }

        public void Remove(object info)
        {

            if (start.GetInfo() == info)
            {
                start = start.GetNext();
            }
            Node n = start;

            while (n.GetNext() != null)
            {
                if (n.GetNext().GetInfo() == info)
                {
                    n.SetNext(n.GetNext().GetNext());
                    break;
                }
                else
                {
                    n = n.GetNext();
                }
            }
        }

        public void Reverse()
        {
            Node prev = null;
            Node next = null;
            Node n = start;

            while (n != null)
            {
                next = n.GetNext();
                n.SetNext(prev);
                prev = n;
                n = next;
            }
            start = prev;
        }

        public void ShowList()
        {

            if (start == null)
            {
                Console.WriteLine("List is empty");
            }
            else
            {
                Node n = start;
                Console.WriteLine(n.GetInfo());
                while (n.GetNext() != null)
                {
                    n = n.GetNext();
                    Console.WriteLine(n.GetInfo());
                }
                Console.WriteLine("That's all!");
            }
        }
    }
}
