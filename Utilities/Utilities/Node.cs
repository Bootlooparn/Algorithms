﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilities
{
    public class Node
    {
        private object info;
        private Node next;
        private Node prev;

        public Node() { }

        public Node(object i)
        {
            try
            {
                info = i;
            }
            catch (ArgumentNullException e)
            {
                Console.WriteLine(e.GetType().Name);
            }
        }

        public void SetInfo(object i)
        {
            info = i;
        }
        public void SetNext(Node n)
        {
            next = n;
        }
        public void SetPrev(Node n)
        {
            prev = n;
        }
        public object GetInfo()
        {
            return info;
        }
        public Node GetNext()
        {
            return next;
        }
        public Node GetPrev()
        {
            return prev;
        }
    }
}
