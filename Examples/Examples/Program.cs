﻿using System;
using DataStructures;
using SortAlgorithms;

namespace Examples
{
    class Program
    {
        static void Main(string[] args)
        {

            SelectionSort sort = new SelectionSort();

            int[] array = new int[]{10, 2, 3, 4, 8, 14, 17, 20, 1, 4, 6, 100, 5, 9};

            array = sort.Sort(array);

            for (int i = 0; i < array.Length; i++)
            {
                Console.WriteLine(array[i]);
            }
            Console.ReadLine();
        }
    }
}
